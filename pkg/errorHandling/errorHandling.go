package errorhandling

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/nathanpbrophy/pipal/pkg/log"
)

func Wrap(base error, wrapper string, args ...string) error {
	return fmt.Errorf("%s: %w", fmt.Sprintf(wrapper, args), base)
}

func AbortWithError(err error, status int, c *gin.Context) {
	log.ErrorLogger.Error(err)
	c.AbortWithStatus(status)
}
