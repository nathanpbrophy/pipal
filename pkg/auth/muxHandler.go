package auth

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gitlab.com/nathanpbrophy/pipal/pkg/constants"
	errorhandling "gitlab.com/nathanpbrophy/pipal/pkg/errorHandling"
	"gitlab.com/nathanpbrophy/pipal/pkg/log"
)

func Login(c *gin.Context) {
	auth := &AuthV1{}
	if err := auth.Load(); err != nil {
		errorhandling.AbortWithError(err, http.StatusUnauthorized, c)
		return
	}

	testAuth := &AuthV1{}
	if err := c.BindJSON(testAuth); err != nil {
		errorhandling.AbortWithError(err, http.StatusUnauthorized, c)
		return
	}

	if !auth.PasswordValid([]byte(testAuth.Password)) {
		c.Status(http.StatusUnauthorized)
		return
	}
	log.Logger.Info("updating session data")
	sess := sessions.Default(c)
	sess.Set(constants.SESSION_KEY_PASSWORD, testAuth.Password)
	if err := sess.Save(); err != nil {
		log.ErrorLogger.Error(err)
	}
	c.Status(http.StatusOK)
}
