package auth

import (
	"gitlab.com/nathanpbrophy/pipal/pkg/filesystem"
	"golang.org/x/crypto/bcrypt"
)

const (
	DataStoreFileName string = "auth"
)

var (
	LoadedAuth Auth = &AuthV1{}
)

func HashAndSaltPassword(pwd []byte) ([]byte, error) {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	return hash, nil
}

type Auth interface {
	PasswordValid([]byte) bool
	GetHashedPassword() []byte
	SetHashedPassword([]byte) error

	filesystem.DataStoreObject
}

type AuthV1 struct {
	Password string `json:"password"`
}

func (a *AuthV1) Load() error {
	return filesystem.LoadGenericDSO(a, DataStoreFileName)
}

func (a *AuthV1) Save() error {
	return filesystem.SaveGenericDSO(a, DataStoreFileName)
}

func (a *AuthV1) GetHashedPassword() []byte {
	if a == nil {
		return []byte{}
	}
	return []byte(a.Password)
}

func (a *AuthV1) SetHashedPassword(in []byte) error {
	if a == nil {
		return nil
	}
	hashed, err := HashAndSaltPassword(in)
	if err != nil {
		return err
	}
	a.Password = string(hashed)
	return nil
}

func (a *AuthV1) PasswordValid(tester []byte) bool {
	stored := a.GetHashedPassword()
	err := bcrypt.CompareHashAndPassword(stored, tester)
	return err == nil
}

func init() {
	_ = LoadedAuth.Load()
}
