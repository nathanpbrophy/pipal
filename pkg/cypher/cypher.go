package cypher

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"

	"github.com/gin-contrib/sessions"
	"gitlab.com/nathanpbrophy/pipal/pkg/constants"
	errorhandling "gitlab.com/nathanpbrophy/pipal/pkg/errorHandling"
)

const AES_KEY_MAX_LENGTH = 32

func deriveKey(s sessions.Session) ([]byte, error) {
	sessionPwd := s.Get(constants.SESSION_KEY_PASSWORD)
	if sessionPwd == nil {
		return nil, fmt.Errorf("cannot retrieve session password, therefore cannot derive an AES key")
	}
	pwd := []byte(sessionPwd.(string))
	hasher := sha256.New()
	hasher.Write(pwd)
	key := hasher.Sum(nil)

	return key[:AES_KEY_MAX_LENGTH], nil
}

func Encrypt(in []byte, s sessions.Session) ([]byte, error) {
	var encrypted []byte
	key, err := deriveKey(s)
	if err != nil {
		return encrypted, errorhandling.Wrap(err, "cannot derive AES key from stored password hash")
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return encrypted, errorhandling.Wrap(err, "cannot encrypt data with key")
	}
	b64 := base64.StdEncoding.EncodeToString(in)
	cipherHolder := make([]byte, aes.BlockSize+len(b64))
	contentIV := cipherHolder[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, contentIV); err != nil {
		return encrypted, errorhandling.Wrap(err, "cannot randomize aes cipher string")
	}
	encryptor := cipher.NewCFBEncrypter(block, contentIV)
	encryptor.XORKeyStream(cipherHolder[aes.BlockSize:], []byte(b64))
	encrypted = cipherHolder

	return encrypted, nil
}

func Decrypt(in []byte, s sessions.Session) ([]byte, error) {
	var decrypted []byte
	key, err := deriveKey(s)
	if err != nil {
		return decrypted, errorhandling.Wrap(err, "cannot derive AES key from stored password hash")
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return decrypted, errorhandling.Wrap(err, "cannot decrypt data with key")
	}
	if len(in) < aes.BlockSize {
		return decrypted, fmt.Errorf("cipher text is too short, cannot decrypt")
	}
	cipherIV := in[:aes.BlockSize]
	text := in[aes.BlockSize:]
	decryptor := cipher.NewCFBDecrypter(block, cipherIV)
	decryptor.XORKeyStream(text, text)
	decrypted, err = base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return decrypted, errorhandling.Wrap(err, "cannot b64 decode the cipher text")
	}

	return decrypted, nil
}
