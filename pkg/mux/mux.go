package mux

import (
	"crypto/rand"
	"fmt"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
	"gitlab.com/nathanpbrophy/pipal/pkg/auth"
	"gitlab.com/nathanpbrophy/pipal/pkg/constants"
	"gitlab.com/nathanpbrophy/pipal/pkg/filesystem"
	"gitlab.com/nathanpbrophy/pipal/pkg/secrets"
)

const (
	sessionKeyFileName = "session.aes"
)

var (
	getters = map[string]gin.HandlerFunc{
		"secrets": secrets.ListSecrets,
		"secret":  secrets.GetSecret,
	}
	creators = map[string]gin.HandlerFunc{
		"secret": secrets.CreateSecret,
		"login":  auth.Login,
	}
	updaters = map[string]gin.HandlerFunc{}
)

func registerRoute(
	registrar func(string, ...gin.HandlerFunc) gin.IRoutes,
	handlerMap map[string]gin.HandlerFunc,
) {
	for route, handle := range handlerMap {
		registrar(fmt.Sprintf("/%s", route), handle)
	}
}

func Router() *gin.Engine {
	router := gin.Default()
	router.HandleMethodNotAllowed = true

	store := memstore.NewStore(getStoredAESKey())
	store.Options(sessions.Options{MaxAge: 60 * 60 * 24})
	router.Use(sessions.Sessions(constants.SESSION_NAME, store))

	registerRoute(router.GET, getters)
	registerRoute(router.POST, creators)
	registerRoute(router.PUT, updaters)

	return router
}

func getStoredAESKey() []byte {
	key, _ := filesystem.Read(sessionKeyFileName)
	return key
}

func generateAESKeyRandom() {
	key := make([]byte, 64)
	rand.Read(key)
	filesystem.Write(sessionKeyFileName, key)
}

func init() {
	generateAESKeyRandom()
}
