package constants

const (
	NIL           = "<nil>"
	APP_NAME      = "pocket_pal"
	APP_DIR       = "." + APP_NAME
	LOG_DIR_NAME  = "log"
	DATA_DIR_NAME = "data"
	MAX_LOG_DIRS  = 7
)

const (
	SESSION_NAME         = "session"
	SESSION_KEY_PASSWORD = "password"
)
