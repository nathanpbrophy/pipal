package log

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/nathanpbrophy/pipal/pkg/constants"
	"gitlab.com/nathanpbrophy/pipal/pkg/setup"
)

var (
	Logger      *logrus.Logger
	ErrorLogger *logrus.Logger
)

func cleanupLogDir() {
	dirs, _ := os.ReadDir(filepath.Join(setup.AppDirPath, constants.LOG_DIR_NAME))
	if len(dirs) < constants.MAX_LOG_DIRS {
		return
	}

	logDirs := make([]string, 0, len(dirs))
	for _, di := range dirs {
		if !di.IsDir() {
			continue
		}
		logDirs = append(logDirs, di.Name())
	}
	sort.Sort(sort.Reverse(sort.StringSlice(logDirs)))
	for _, di := range logDirs[:constants.MAX_LOG_DIRS] {
		if di == "" {
			continue
		}
		fmt.Println("removing log directory", di)
		os.RemoveAll(di)
	}
}

func getLogFileName() string {
	now := time.Now()
	nowFmt := strings.ReplaceAll(strings.ReplaceAll(now.Format(time.DateTime), " ", "_"), ":", "_")
	dayStamp := now.Format(time.DateOnly)
	logFileName := fmt.Sprintf("app_%s.log", nowFmt)
	fullLogPath := filepath.Join(setup.AppDirPath, constants.LOG_DIR_NAME, dayStamp, logFileName)
	os.MkdirAll(filepath.Dir(fullLogPath), 0755)

	return fullLogPath
}

func init() {
	cleanupLogDir()
	lf, _ := os.OpenFile(getLogFileName(), os.O_CREATE|os.O_APPEND|os.O_RDWR, 0660)
	mw := io.MultiWriter(os.Stdout, lf)
	mwe := io.MultiWriter(os.Stderr, lf)
	fmter := &logrus.TextFormatter{
		ForceColors:     true,
		DisableColors:   false,
		FullTimestamp:   true,
		TimestampFormat: time.RFC3339Nano,
		PadLevelText:    true,
	}
	Logger = logrus.New()
	Logger.SetReportCaller(true)
	Logger.SetOutput(mw)
	Logger.SetLevel(logrus.DebugLevel)
	Logger.SetFormatter(fmter)

	ErrorLogger = logrus.New()
	ErrorLogger.SetReportCaller(true)
	ErrorLogger.SetOutput(mwe)
	ErrorLogger.SetLevel(logrus.ErrorLevel)
	ErrorLogger.SetFormatter(fmter)
}
