package secrets

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	errorhandling "gitlab.com/nathanpbrophy/pipal/pkg/errorHandling"
	"gitlab.com/nathanpbrophy/pipal/pkg/log"
)

func loadSecrets(c *gin.Context) SecretList {
	secrets := &SecretV1List{}
	err := secrets.Load()
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			errorhandling.AbortWithError(err, http.StatusInternalServerError, c)
			return nil
		}
	}
	return secrets
}

func ListSecrets(c *gin.Context) {
	secrets := loadSecrets(c)
	if secrets == nil {
		secrets = &SecretV1List{}
	}
	c.IndentedJSON(http.StatusOK, secrets)
}

func GetSecret(c *gin.Context) {
	val, ok := c.GetQuery(getSecretQueryKey)
	if !ok {
		errorhandling.AbortWithError(fmt.Errorf("the query key %q is required", getSecretQueryKey), http.StatusBadRequest, c)
		return
	}
	log.Logger.Infof("attempting to get secret by name %q", val)
	secrets := loadSecrets(c)
	if secrets == nil {
		return
	}
	secret := secrets.GetSecret(val)
	if secret == nil {
		errorhandling.AbortWithError(fmt.Errorf("no secret querable via name %q could be found", val), http.StatusNotFound, c)
		return
	}

	if val, ok := c.GetQuery(getSecretPlainTextQueryKey); ok {
		if parsed, _ := strconv.ParseBool(val); parsed {
			secret.RenderPlaintext(sessions.Default(c))
		}
	}
	c.IndentedJSON(http.StatusOK, secret)
}

func CreateSecret(c *gin.Context) {
	newSecret := &SecretV1{}

	if err := c.BindJSON(newSecret); err != nil {
		log.ErrorLogger.Error(err)
		return
	}

	if err := newSecret.SetValue(newSecret.Value, sessions.Default(c)); err != nil {
		errorhandling.AbortWithError(err, http.StatusInternalServerError, c)
		return
	}

	secrets := loadSecrets(c)
	if secrets == nil {
		return
	}

	secrets.AddSecret(newSecret)
	if err := secrets.Save(); err != nil {
		errorhandling.AbortWithError(err, http.StatusInternalServerError, c)
		return
	}
	c.IndentedJSON(http.StatusOK, secrets)
}
