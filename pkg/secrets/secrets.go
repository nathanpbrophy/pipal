package secrets

import (
	"github.com/gin-contrib/sessions"
	"gitlab.com/nathanpbrophy/pipal/pkg/constants"
	"gitlab.com/nathanpbrophy/pipal/pkg/cypher"
	errorhandling "gitlab.com/nathanpbrophy/pipal/pkg/errorHandling"
	"gitlab.com/nathanpbrophy/pipal/pkg/log"
)

const (
	getSecretQueryKey          string = "name"
	getSecretPlainTextQueryKey string = "plaintext"

	DataStoreFileName string = "secrets"
)

type SecretType int

const (
	Password SecretType = iota
)

type Secret interface {
	GetName() string
	GetValue(sessions.Session) ([]byte, error)
	RenderPlaintext(sessions.Session) error

	SetName(string)
	SetValue([]byte, sessions.Session) error
}

type SecretV1 struct {
	Type      SecretType `json:"type"`
	Name      string     `json:"name"`
	Value     []byte     `json:"value"`
	Plaintext []byte     `json:"plaintext,omitempty"`
}

func (s *SecretV1) RenderPlaintext(sess sessions.Session) error {
	if s == nil {
		return nil
	}

	pt, err := s.GetValue(sess)
	if err != nil {
		return err
	}
	s.Plaintext = pt
	return nil
}

func (s *SecretV1) GetName() string {
	if s == nil {
		return constants.NIL
	}
	return s.Name
}

func (s *SecretV1) GetValue(sess sessions.Session) ([]byte, error) {
	if s == nil {
		return []byte(constants.NIL), nil
	}

	decrypted, err := cypher.Decrypt(s.Value, sess)
	if err != nil {
		e := errorhandling.Wrap(err, "cannot decrypt secret data for %s", s.GetName())
		log.ErrorLogger.Error(e)
		return nil, e
	}

	return decrypted, nil
}

func (s *SecretV1) SetName(name string) {
	if s == nil {
		s = &SecretV1{}
	}
	s.Name = name
}

func (s *SecretV1) SetValue(value []byte, sess sessions.Session) error {
	if s == nil {
		s = &SecretV1{}
	}
	encrypted, err := cypher.Encrypt(value, sess)
	if err != nil {
		e := errorhandling.Wrap(err, "cannot encrypt secret data for %s", s.GetName())
		log.ErrorLogger.Error(e)
		return e
	}
	s.Value = encrypted
	return nil
}
