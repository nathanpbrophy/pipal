package secrets

import (
	"fmt"
	"sync"

	"gitlab.com/nathanpbrophy/pipal/pkg/filesystem"
)

type SecretList interface {
	AddSecret(Secret) error
	ListSecrets() []Secret
	GetSecret(string) Secret
	GetCount() int

	filesystem.DataStoreObject
}

type SecretV1List struct {
	Count int         `json:"count"`
	Items []*SecretV1 `json:"items"`
}

func (s *SecretV1List) Load() error {
	return filesystem.LoadGenericDSO(s, DataStoreFileName)
}

func (s *SecretV1List) Save() error {
	return filesystem.SaveGenericDSO(s, DataStoreFileName)
}

func (s *SecretV1List) GetSecret(name string) Secret {
	if s == nil {
		return nil
	}
	var (
		found Secret = nil
		wg    sync.WaitGroup
	)

	cl := func(in Secret) {
		defer wg.Done()
		if in.GetName() == name {
			found = in
		}
	}

	for _, secret := range s.Items {
		wg.Add(1)
		go cl(secret)
	}

	wg.Wait()
	return found
}

func (s *SecretV1List) AddSecret(in Secret) error {
	if s == nil {
		s = &SecretV1List{}
	}
	casted, ok := in.(*SecretV1)
	if !ok {
		return fmt.Errorf("failed to add secret to list")
	}
	s.Count = s.Count + 1
	s.Items = append(s.Items, casted)
	return nil
}

func (s *SecretV1List) ListSecrets() []Secret {
	if s == nil {
		return []Secret{}
	}
	items := make([]Secret, 0, s.Count)
	for _, in := range s.Items {
		items = append(items, in)
	}
	return items
}

func (s *SecretV1List) GetCount() int {
	if s == nil {
		return 0
	}
	return s.Count
}
