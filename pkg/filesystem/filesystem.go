package filesystem

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"reflect"

	"gitlab.com/nathanpbrophy/pipal/pkg/constants"
	errorhandling "gitlab.com/nathanpbrophy/pipal/pkg/errorHandling"
	"gitlab.com/nathanpbrophy/pipal/pkg/log"
	"gitlab.com/nathanpbrophy/pipal/pkg/setup"
)

type DataStoreObject interface {
	Load() error
	Save() error
}

func getTypeForInterface(in interface{}) string {
	var kind string
	if t := reflect.TypeOf(in); t.Kind() == reflect.Ptr {
		kind = t.Elem().Name()
	} else {
		kind = t.Name()
	}
	return kind
}

func LoadGenericDSO(in interface{}, dsf string) error {
	if in == nil {
		return nil
	}

	kind := getTypeForInterface(in)
	log.Logger.Infof("preparing to load DSO for type %s", kind)
	data, err := ReadDataStoreObject(dsf)
	if err != nil {
		return errorhandling.Wrap(err, "cannot load the %s from filesystem", kind)
	}
	if err := json.Unmarshal(data, in); err != nil {
		return errorhandling.Wrap(err, "cannot unmarshal json data to a %s type", kind)
	}
	return nil
}

func SaveGenericDSO(in interface{}, dsf string) error {
	if in == nil {
		return nil
	}

	kind := getTypeForInterface(in)
	log.Logger.Infof("preparing to save DSO for type %s", kind)
	data, err := json.Marshal(in)
	if err != nil {
		return errorhandling.Wrap(err, "cannot marshal json data for %s", kind)
	}
	if err := WriteDataStoreObject(dsf, data); err != nil {
		return errorhandling.Wrap(err, "cannot save dso file for", kind)
	}
	return nil
}

func normalizeFilePathForAppDir(in string) string {
	return filepath.Join(setup.AppDirPath, in)
}

func Write(filename string, content []byte) error {
	log.Logger.Infof("WRITE %q", filename)

	filename = normalizeFilePathForAppDir(filename)

	return os.WriteFile(filename, content, 0755)
}

func WriteDataStoreObject(filename string, content []byte) error {
	dataPath := filepath.Join(constants.DATA_DIR_NAME, fmt.Sprintf("%s.json", filename))
	return Write(dataPath, content)
}

func Read(filename string) ([]byte, error) {
	log.Logger.Infof("READ %s", filename)

	filename = normalizeFilePathForAppDir(filename)

	return os.ReadFile(filename)
}

func ReadDataStoreObject(filename string) ([]byte, error) {
	dataPath := filepath.Join(constants.DATA_DIR_NAME, fmt.Sprintf("%s.json", filename))
	return Read(dataPath)
}
