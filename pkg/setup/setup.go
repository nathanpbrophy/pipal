package setup

import (
	"os"
	"path/filepath"

	"gitlab.com/nathanpbrophy/pipal/pkg/constants"
)

var AppDirPath string

func PrepareAppDir() string {
	appDir, err := os.UserHomeDir()
	if err != nil {
		appDir = "."
	}

	appDir = filepath.Join(appDir, constants.APP_DIR)
	os.MkdirAll(appDir, 0755)

	logDir := filepath.Join(appDir, constants.LOG_DIR_NAME)
	os.MkdirAll(logDir, 0755)

	dataDir := filepath.Join(appDir, constants.DATA_DIR_NAME)
	os.MkdirAll(dataDir, 0755)
	return appDir
}

func init() {
	AppDirPath = PrepareAppDir()
}
