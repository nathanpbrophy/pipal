# PiPal

- [PiPal](#pipal)
  - [Getting started](#getting-started)
  - [Test and Deploy :construction:](#test-and-deploy-construction)
  - [Project status](#project-status)

The PiPal is a rasbian based software project that runs on a Raspberry Pi 4, and is meant to be a desktop aid.  Some features that are targeted from this are:

1. Local password management, so you don't need to write it on sticky notes anymore
2. Ambient lighting module for when you need to get in the zone
3. A spotify plugin for allowing multiple folks to control what song is playing
4. More, coming soon...

## Getting started

To get started with the project, simply clone it and `go run .`.  Formal CI/CD processes are under construction.

## Test and Deploy :construction:

Under construction.

***

## Project status

:construction:
