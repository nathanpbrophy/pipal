package main

import (
	"gitlab.com/nathanpbrophy/pipal/pkg/log"
	"gitlab.com/nathanpbrophy/pipal/pkg/mux"
)

func main() {
	log.Logger.Info("starting application...")

	engine := mux.Router()
	engine.Run("localhost:8080")
}
